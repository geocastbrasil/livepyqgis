# Criando Atributos

Trabalhar com vetores não significa que vamos trabalhar apenas com poligonos. Precisamos também de atribuir valores para esses vetores que nos ajudaram a retirar informações, identificar classes e por ai vai.  
Os atributos nos permitem saber, a área delimitada por um poligono, uma extensão de uma cerca, os dado do proprietario daquela propriedade, as classes de vegetação de uma classificação feita.  
Hoje vamos tratar de criar campos em camadas criadas do zero, depois a gente vê como criar e manipular em camadas que já existem campos, e já estão criadas.


## Os campos da tabela

Os campos da tabela do Qgis são objetos do tipo [`QgsField()`](https://qgis.org/pyqgis/3.4/core/QgsField.html?highlight=qgsfield#module-QgsField) que recebem os seguintes parâmetros:

* __name__ - Nome do campo;
* __type__ - Tipo do campo;
* __typeName__ - String do nome do campo;
* __len__ - Tamanho do campo;
* __prec__ - Precisão do campo;
* __comment__ - Algum comentário que possa acopanhar o campo;
* __subType__ - É utilizando quando temos uma coleção de dentro do campo, e quando cada elemento dessa coleção deve ter o mesmo tipo.

Para o tipo de um campo pode ser definido pelo `QVariant` que é uma classe do `PyQt5.Core" que deve ser importada em scripts standalone.  
Basicamente vamos usar os tipos:

* `QVariant.Int` - Para Inteiros;
* `QVariant.Double` - Para Decimais;
* `QVariant.String` - Para Campos de Texto.

## Criando campos

Criamos campos da mesma forma que criamos um vetor, primeiro criamos o campos, e em seguida adicionamos esse campo a uma camada:

```Python
campo = QgsField('nome', QVariant.String)
camada.dataProvider.addAttributes([campo]) # O método addAttributes precisa de uma lista de campos a serem inseridos na camada.
camada.updateFields()
```

## Populando os campos

Para Preencher os campos criados na tabela de atributos, primeiro, temos de instanciar a feição já com os campos dentro dela, da seguinte forma:

```Python
feicao = QgsFeature(camada.fields()) # Cria a feição e inicia os campos dentro dela.
```

Agora basta popular os campos:

```Python
feicao.setGeometry(geometria) # Adiciona a geometria
feicao.setAttributes(['Dado inserido no campo']) # Passando uma lista de atributos para serem inseridos nos campos.
camada.dataProviedr().addFeature(feicao) # Adiciona a feição à camada.
camada.updateExtents()
```
