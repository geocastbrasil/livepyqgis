# Live 5: Manipulação de dados raster  

**[![Live pyqgis 8 - Manipulação de dados raster](https://img.youtube.com/vi/I30XDtzUQQo/0.jpg)](https://www.youtube.com/watch?v=I30XDtzUQQo)**  

[Live da Issue 15](https://gitlab.com/geocastbrasil/livepyqgis/issues/15)  

Para fazer um contra-ponto às duas últimas lives (live [6](/estudos/live_6/README.md) & [7](/estudos/live_7/README.md)), que trataram exclusivamente de dados vetoriais, vamos dedicar essa live ao estudo da manipulação de dados raster.  
Os dados usados foram organizados especificamente para essa live e por isso, vocês terão que baixar-los [desta pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing). Basta baixar a pasta `dados` e incluí-la tal como está no repositório `livepyqgis`.  
> :warning: Mesmo que já tenha feito isso para as lives anteriores, confirme a existência da pasta "**PARNASO**", na pasta `livedepyqgis/dados/imagem_satelite/LS8/`.  

Vamos trabalhar com o arquivo `L8_20180901_cortado.tif`. Para mais informações a respeito da origem desse dados, vocês podem acessar o metadado da imagem original.  

**De que se trata essa imagem :question:**  
É um arquivo com quatro bandas (banda 2: vermelho, banda 3: verde, banda 4: azul e banda 5: infravermelho) do satélite Landsat 8, já com o préprocessamento realizado (conversão de ND a reflectancia e *pansharpening*, usando o *Semi-automatic classification plugin* (SCP) e cortado para o limite do Parque Nacional Serra dos Órgãos. 

## Carregando a imagem ao projeto  
Ainda que já tenhamos apresentado como carregar uma imagem ao projeto, vamos fazê-lo de novo, aproveitando parte do script apresentado [na live 5 - Criando um script standalone](/estudos/live_5/README.md):  

```python
from os import path
from qgis.PyQt import QtWidgets # Para abrir uma janela de erro para o usuario.
# definir path ao diretorio livedepyqgis
diretorio = "path_to_livepyqgis"

# Complementando o caminho até o arquivo raster
full_path = path.join(diretorio, "dados/imagem_stelite/LS8/PARNASO/L8_20180901_cortado.tif")
```  

Até aqui, nada de novo: carregamos os módulos que vamos usar: `os` e `PyQt`, do qual vamos usar apenas o método `QtWidgets` para apresentar uma janela de erro, caso a importação do raster não seja bem sucedida. Caso o arquivo exista, o mesmo será carregado com a função/classe [`QgsRasterLayer()`](https://qgis.org/pyqgis/master/core/QgsRasterLayer.html?highlight=qgsrasterlayer#module-QgsRasterLayer) (já apresentada em outras lives) e atribuído ao objeto `rasterLayer` (instância da classe `qgsRasterLayer`). Em seguida, o adicionamos ao projeto, usando o método `mapAddLayer()`, da classe `QgsPrject` (que deve ser instanciado antes) - ponto já apresentado em lives anteriores-, conforme o codigo que segue:  

```python
# Verificando se o arquivo existe na pasta, antes de adicionar ao QGIS
if path.isfile(full_path):
    # em existindo, cria-se instancia QgsRasterLayer com a img
    rasterLayer = QgsRasterLayer(full_path, 'teste')
    # Pegando o provedor de dados para configurar o simbologia, transparência, etc.
    provider = rasterLayer.dataProvider()
    # adiciona ao projeto
    QgsProject.instance().addMapLayer(rasterLayer)
else:
    # Caso o arquivo não exista, mostrar uma mensagem de erro.
    QtWidgets.QMessageBox.critical(None, "Erro ao adicionar raster.", "Confirme o path informado")
```  

**Sobre a classe QgsRasterLayer, insistimos: leia a documentação**. Com essa classe, automaticamente várias tarefas são executadas:
Verás que essa classe não só carrega o raster, mas também facilita nossa vida em vários quesitos.  

- Carrega o estilo padrão do raster(.qml), caso existente;  
- Cria `RasterStatsVector` com valores iniciais de cada band;  
- Calcula a extensão do raster;  
- Identifica se o raster é do tipo "gray", "paletted" or "multiband";  
- Caso seja um raster "multiband", designa bandas para os canais do vermelho (red), verde (green), azul (blue) e cinza (gray);  

Por mais que estejamos usando o terminal python do QGIS, que nos permite visualizar a imagem carregada ao projeto e, logo, diferenciar seu tipo e principais características, é importante saber que, a partir do momento que uma camada raster é carregada, podemos identificar o seu tipo, quantidade de bandas, dentre otras características. Isso tudo usando métodos ou acessando atributos da classe `QgsRasterLayer`:

### Tipo de camada

Um exemplo disso, é a identificação do tipo de raster incorporado ao projeto, com o método [`rasterType()`](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterLayer.html#qgis.core.QgsRasterLayer.rasterType):  

```python
rasterLayer.rasterType()
```

* Multiband = 2  
* Palette = 1  

### Quantidade de bandas da imagem

Além disso, como para adicionar o raster ao projeto usamos a classe `QgsRasterLayer`, independente do raster ser de banda única (`singleband`) ou múltiplas bandas (`multiband`), podemos identifica a quantidade de bandas existentes com o método [`bandCount`](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterLayer.html?highlight=qgsrasterlayer%20layertype#qgis.core.QgsRasterLayer.bandCount):  

```python
# quantas bandas possui
rasterLayer.bandCount()
```  

## Sobre renderização do raster

O `renderer()` define a forma como a imagem será renderizada no *canvas*. Por isso, para conhecer quais bandas estão sendo utilizadas na visualização, bem como para identificar o estilo de visualização da imagem, vamos trabalhar com métodos do [`renderer()`](https://qgis.org/pyqgis/master/core/QgsRasterRenderer.html#qgis.core.QgsRasterRenderer). Para identificar o tipo de `renderer` atual, podemos usar o método [`type()`](https://qgis.org/pyqgis/master/core/QgsRasterLayer.html?highlight=qgsrasterlayer#qgis.core.QgsRasterLayer.LayerType) (GrayOrUndefined, Palette or Multiband):

```python
rasterLayer.renderer().type()
```

No método `renderer.type()` teremos a informação que o tipo de renderização é [`QgsMultiBandColorRenderer`](https://qgis.org/pyqgis/master/core/QgsMultiBandColorRenderer.html#module-QgsMultiBandColorRenderer).  

Como já sabemos quantas bandas temos no raster adicionado, podemos, com o método [`usesBands()`](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterRenderer.html?highlight=renderer%20usesbands#qgis.core.QgsRasterRenderer.usesBands) da classe `renderer`, identificar quais bandas do raster estão direcionadas nas canal do vermelho, verde e azul:  

```python
# quais estão sendo usadas
rasterLayer.renderer().usesBands()
```

O método `renderer.usesBands()` nos retorna uma lista das bandas na ordem que estão sendo utilizadas (lembre-se, R/G/B).

:warning: Saber como identificar esses atributos é importante pois, como vocês viram, o processo de incorporação de um dado raster é o mesmo independente de ele ser um "SingleBandRasterLayer" (raster composto por única banda) ou "MultiBandRasterLayer" (raster composto por mais de uma banda). E caso estejamos trabalhando com um script *standalone*, podemos usar esse atributo para checar o tipo de raster carregado e decidir, dentre várias coisas, como representá-lo.  

Como vimos, os dados raster possuem formas específicas de visualização. Ao todos, são 8 possibilidades, definidas pelo atributo `renderer().type()`, no `QgsRasterLayer()`:

- SingleBandGray -> Um raster *singleband* é apresentado com a variação dos seus valores em escala de tons de cinza (0-255);  
- SingleBandPseudoColor -> Um raster é representado usando um algoritmo *pseudocolor*;  
- PalettedSingleBandGray -> Uma camada *PaletteD* é representada e escala de tons de cinza;  
- PalettedSingleBandPseudoColor -> Uma camada *PaletteD* é apresentada por uma de suas bandas como *pseudo color*;  
- PalettedMultiBandColor -> Uma camada *PaletteD* que possua informações de cores 24bit and 8 bits são representadas em cor;  
- MultiBandSingleBandGray -> Um raster que possua duas ou mais bandas, mas apenas uma é usada e representada em escala de tons de cinza;  
- MultiBandSingleBandPseudoColor -> Uma camada que possua duas ou mais bandas, mas tendo apenas uma em uso e representada em *pseudocolor*;  
- MultiBandColor -> Uma camada que possua duas ou mais bandas, mapeadas  nos canais RGB. Caso seja uma multibanda de apenas duas bandas, uma delas será mapeada em mais de um canal de cor;  

Alguns dos estilos mencionados, dependem de estatísticas sobre a camada, como valores min / max / mean / stddev etc. Veremos mais a diante como obter tais valores usando a função `bandStatistics`.  

## Ajustando contraste da imagem

Como incorporamos um raster com quatro bandas, o `QgsRasterLayer` já percebe e o carrega ao projeto como multiband, e com bandas designadas aos canais de cor do vermelho, verde e azul. Vamos explorar um pouco o sistema de visualização dos dados raster, como definir ajuste de contraste, definição das bandas de composição e etc.  

É provável que vc veja a imagem sem ajuste de contraste. Para aplicar o contraste padrão (ajustar aos valores Min e Max com ajuste acumulativo), basta usar o método [`setDefaultContrastEnhancement()`](https://qgis.org/pyqgis/3.0/core/Raster/QgsRasterLayer.html?highlight=setdefaultcontrastenhancement#qgis.core.QgsRasterLayer.setDefaultContrastEnhancement).  

```python
rasterLayer.setDefaultContrastEnhancement()
```  

Contudo, há vários outros métodos de ajuste de contraste. Podemos definir o ajuste de contraste usando o método [`setContrastEnhancement()`](https://qgis.org/pyqgis/master/core/QgsRasterLayer.html?highlight=setdefaultcontrastenhancement#qgis.core.QgsRasterLayer.setContrastEnhancement), ao qual teremos que informar o [algoritmo](https://qgis.org/pyqgis/master/core/QgsContrastEnhancement.html#qgis.core.QgsContrastEnhancement.ContrastEnhancementAlgorithm) de ajuste a ser usado:  

* Sem realce (*NoEnhancement*) = 0;  
* Ajustar ao Min e Max (*StretchToMinimumMaximum*) = 1;  
* Ajustar e cortar ao Min e Max (*StretchAndClipToMinimumMaximum*) = 2;  
* Cortar ao Min e Max (*ClipToMinimumMaximum*) = 3;  
* Ajuste definido pelo usuário (*UserDefinedEnhancement*) = 4;  

Vamos prová-los:  

```python
# Brincando com o contraste
rasterLayer.setContrastEnhancement( 0 )
rasterLayer.setContrastEnhancement( 1 )
rasterLayer.setContrastEnhancement( 2 )
```  

Outra opção é definir o algorítmo a partir dos atributos da classe [`QgsContrastEnhancement`](https://qgis.org/pyqgis/3.0/core/Contrast/QgsContrastEnhancement.html?highlight=qgscontrastenhancement):  

```python
rasterLayer.setContrastEnhancement(
    QgsContrastEnhancement.ClipToMinimumMaximum
    )
```  

Ou a partir do nome:

```python
rasterLayer.setContrastEnhancement(
    QgsContrastEnhancement.
        contrastEnhancementAlgorithmFromString(
        'StretchToMinimumMaximum'
        )
    )
```  

## Alterando a composição de visualização  

Como carregamos uma MultiBandRasterLayer, podemos definir qual banda deverá ficar em qual canal de visualização. Vamos provar adicionando o infravermelho próximo (banda 4) ao canal do vermelho e incorporar a banda vermelha (banda 1) ao canal do verde.  

>:warning: a pesar de estarmos usando linguagem python (a qual usa como índice para elementos de uma lista valores iniciando desde 0), neste caso, a contagem de bandas começa por 1, pois não está relacionada a contagem/índice de elementos da linguagem python.

E por ser *multibandcolor*, podemos usar os métodos [`QgsMultiBandColorRenderer.setRedBand()`](https://qgis.org/pyqgis/master/core/QgsMultiBandColorRenderer.html?highlight=setredband#qgis.core.QgsMultiBandColorRenderer.setRedBand) e [`QgsMultiBandColorRenderer.setGreenBand()`](https://qgis.org/pyqgis/master/core/QgsMultiBandColorRenderer.html?highlight=setredband#qgis.core.QgsMultiBandColorRenderer.setRedBand). Dentro do parênteses, temos que informar o número da banda a ser usada, e posteriormente o método `rasterLayer.triggerRepaint()` para que o *canvas* (a visualização) seja atualizada:  

>:warning: como vc pode deduzir... existe um método: `QgsMultiBandColorRenderer.setBlueBand()`  

```python
# alterando composição
rasterLayer.renderer().setRedBand(4) # NDVI
rasterLayer.renderer().setGreenBand(1) # vermelho
rasterLayer.triggerRepaint()
```

>:warning: Não se esqueça que, caso seja necessário, altere o ajuste de contraste após a alteração da composição de visualização:
>`rasterLayer.setDefaultContrastEnhancement()`  

Bom, já vimos algumas características importantes dos rasters de tipo multi band. Agora, vamos ver um pouco dos rasters single band... e aproveitando que temos as bandas necessárias para calcular o NDVI:  

## Raster calculator (*gdal:rastercalculator*)  

Vamos usar o *processing* para calcular o NDVI com o algoritmo *gdal*. Como ele podemos usar quantas bandas quisermos para o cálculo (:warning: Não esqueça de fazer o import dele!). Basta ir adicionando "INPUT_A", "INPUT_B", "INPUT_...", "INPUT_Z" e definir a banda a cada raster usado com um valor numérico: "BAND_A", "BAND_B", "BAND_...", "BAND_Z".  

Para mais informações: `processing.algorithmHelp("gdal:rastercalculator")`  

>:warning: a pesar de estarmos usando linguagem python (a qual usa como índice para elementos de uma lista valores iniciando desde 0), neste caso, a contagem de bandas começa por 1, pois não está relacionada a contagem/índice de elementos da linguagem python.

```python
# raster calc
import processing

processing.runAndLoadResults('gdal:rastercalculator',
    {'INPUT_A': rasterLayer,
    'BAND_A': 4,
    'INPUT_B': rasterLayer,
    'BAND_B': 1,
    'FORMULA': '(A-B)/(A+B)',
    'OUTPUT' : '/tmp/OUTPUT.tif',
    'RTYPE' : 5})
NDVI = QgsProject.instance().mapLayersByName('Calculated')[0]
```

Logo após executar o algoritmo, o raster será carregado. Por isso, logo após, instanciamos um objeto (NDVI) a partir do nome da layer ([`mapLayersByName()`](https://qgis.org/pyqgis/3.2/core/Project/QgsProject.html?highlight=maplayersbyname#qgis.core.QgsProject.mapLayersByName). Note que o QGIS usado para essa live está em inglês e isso fará com que a layer seja carregada com o nome em inglês (calculated). Caso o seu QGIS esteja em outra lingua, atente a esse detalhe!  

## Alterando visualização  

A Layer NDVI foi carregada e se trata de um raster de banda única, *single band*. Para definir o tipo de visualização de um raster *single band*, é um pouco diferente do *multi band*. Mas como se trata de um `QgsRasterLayer`, podemos acessar o tipo de renderização, usando o mesmo atributo apresentado antes (`renderer().type()`). O resultado será `singlebandgray`, já que estamos com um raster single band e usando rampa de cor de tons de cinza, que é o padrão adotado pelo QGIS.  

```python
NDVI.renderer().type()
# singleband color ramp
```

Antes de mudar o tipo de renderização, vamos calcular algumas estatísticas básicas do raster, como valor mínimo, médio e valor máximo.

### Calculando estatísticas  

Para poder calcular as estatísticas do raster, precisamos instanciar um objeto com o [`dataProvider()`](https://qgis.org/pyqgis/master/core/QgsDataProvider.html?highlight=dataprovider#module-QgsDataProvider). Já vimos isso antes!  
 
```python
provider = NDVI.dataProvider()
```

E, então, calculamos as estatísticas com [`bandStatistics()`](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterInterface.html?highlight=bandstatistics#qgis.core.QgsRasterInterface.bandStatistics), informando numericamente a banda a ter a estatística calculada:  

```python
# Sobre estatístics
min = provider.bandStatistics(1).minimumValue
mean = provider.bandStatistics(1).mean
max = provider.bandStatistics(1).maximumValue
```  

Para mais informações a respeito das estatísticas, ver [`QgsRasterBandStats`](https://qgis.org/pyqgis/master/core/QgsRasterBandStats.html#qgis.core.QgsRasterBandStats).  

> :warning: As estatísticas calculadas serão usadas nos passos seguinte  

### Definido uma rampa de cor (na unha, com QgsColorRampShader)  

Para alterar o sistema de visualização de um raster single band, é necessário criar a rampa de cores e os intervalos "na unha". Isto é, cada intervalo e seus cores de referência.  

Primeiro, vamos criar um objeto (`fcn`) da classe [`QgsColorRampShader`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html), que nos permite criar uma rampa de cores a partir de uma `lista` de valores.

:warning: já vimos essa estrutura de dados, chamada de lista, antes!  

Em seguida, a esse objeto `fcn` vamos definir a forma de classificação, anterando o atributo da classe com o método [`setColorRampType`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html?highlight=setcolorramptype#qgis.core.QgsColorRampShader.setColorRampType).  
As opções são:  
* Continuous;  
* Discrete;  
* EqualInterval;  
* Exact;  
* Interpolated;  
* Quantile;  

Para saber qual o tipo de classificação está sendo usado, podemos usar o método [`colorRampTypeAsQString`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html?highlight=colorramptypeasqstring#qgis.core.QgsColorRampShader.colorRampTypeAsQString), que retornará em texto o tipo de classificação, ou pelo método [`classificationMode`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html?highlight=classificationmode#qgis.core.QgsColorRampShader.classificationMode) que retornará um valor inteiro que representa o tipo de classificação;  

Seguindo, enfim criaremos uma lista (:warning: já vimos isso antes ;) que receberá, no caso desse exemplo, três elementos da classe [`QgsColorRampShader`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html?highlight=qgscolorrampshader). A cada elemento, informaremos o valor do *pixel*, a cor, usando `QColor` e definindo valores para RGB, e em formato *string* a label a ser adicionada à legenda.  
Uma vez finalizada a definição dos intervalos, cores e as etiquetas, alteramos o atributo `QgsRampItemList` do nosso objeto `fcn` usando o método [`setColorRampItemList`](https://qgis.org/pyqgis/3.2/core/Color/QgsColorRampShader.html?highlight=setcolorrampitemlist#qgis.core.QgsColorRampShader.setColorRampItemList).  

```python
# Na unha
fcn = QgsColorRampShader()
fcn.setColorRampType(QgsColorRampShader.Interpolated)

lst = [ QgsColorRampShader.ColorRampItem(-1, QColor(255, 0, 0), '-1'), 
    QgsColorRampShader.ColorRampItem(0.7, QColor(255, 255, 192), '0.7'),
    QgsColorRampShader.ColorRampItem(1, QColor(0, 150, 0), '1') ]
fcn.setColorRampItemList(lst)
```

Até aqui estivemos criando e configurando uma rampa de cores. O que precisamos fazer agora é instanciar um objeto com a clase [`QgsRasterShader`](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterShader.html?highlight=qgsrastershader#module-QgsRasterShader). A essa classe, vamos configurar uma rampa de cores. E, claro, é aqui que entra nossa rampa de cores gerada nos passos anteriores. Vamos usar o método [setRasterShaderFunction](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterShader.html?highlight=setrastershaderfunction#qgis.core.QgsRasterShader.setRasterShaderFunction) a qual será passado o objeto `fcn` como parâmetro.  

```python
shader = QgsRasterShader()
shader.setRasterShaderFunction(fcn)
```

Agora o que temos que fazer é instanciar um objeto novo da classe [QgsSingleBandPseudoColorRenderer](https://qgis.org/pyqgis/3.2/core/Single/QgsSingleBandPseudoColorRenderer.html?highlight=qgssinglebandpseudocolorrenderer) que define o tipo de renderização baseado em *pseudo color*. E em seguida, alteramos o tipo de *renderer* do nosso objeto NDVI com o método [setRenderer](https://qgis.org/pyqgis/3.2/core/Raster/QgsRasterLayer.html?highlight=setrenderer#qgis.core.QgsRasterLayer.setRenderer), passando o objeto anterior como parâmetro. Para finalziar a poder visualizar o resultado, ` NDVI.triggerRepaint()`.  

```python
renderer = QgsSingleBandPseudoColorRenderer(NDVI.dataProvider(), 1, shader)
NDVI.setRenderer(renderer)

NDVI.triggerRepaint()
```  

Perceba que nesses três passo, fizemos uma rampa de cores do zero (!),  atribuimos a um `QgsRasterShader`, para poder alterar o `renderer` do NDVI para *pseudo color*, passando esse último objeto às configurações do objeto NDVI.  

O único problema que vocês poderão perceber é que usamos os valores que possivelmente possuem um NDVI: de -1 a 1. Contudo, não é necessáriamente os vamosres mínimos e máximos do nosso raster. Por isso, vamos refazer os passos anteriores, mas dessa vez usando as estatísticas calculadas anteriormente:  

```python
lst = [ QgsColorRampShader.ColorRampItem(min, QColor(255, 0, 0), str(round(min, 2))), 
    QgsColorRampShader.ColorRampItem(mean, QColor(255, 255, 192), str(round(mean, 2))),
    QgsColorRampShader.ColorRampItem(max, QColor(0, 150, 0), str(round(max, 2))) ]
```  

## widgets  

### Gerando Histograma de MultiBandRasterLayer

Para entender melhor a respeito das características das imagens carregadas, vamos gerar o histograma de todas as suas bandas. Para isso, vamos usar o *widget* [`QgsRasterHistogramWidget`](https://qgis.org/pyqgis/3.0/gui/Raster/QgsRasterHistogramWidget.html?highlight=qgsrasterhistogram#module-QgsRasterHistogramWidget). Vamos instanciá-lo e fazer com que apareça como janela, usando o método `show()`. Após isso, temos que calcular o histograma, o que será feito com o método `computeHistogram`. Esse método tem um parâmetro booleano para forçar a estimação. Uma vez estimado, precisamos atualizá-lo usando o método `refreshHistogram`.  

Como o histograma é um gráfico bastante usado no processo de análise exploratória, é possível salvá-lo como imagem (método `histoSaveAsImage`, o qual deverá ser informado com o *path* ao diretório ao qual a imagem deverá ser salva).  

```python
# histograma
h = QgsRasterHistogramWidget(rasterLayer)
h.show()
h.computeHistogram(True)

h.refreshHistogram()
h.histoSaveAsImage(path.join(diretorio, "histograma.png"))
h.destroy()
```

Usamos o método `destroy` para fechar o *widget* de histograma.

### Histograma de SingleBandRasterLayer

Bom, agora que temos o NDVI calculado, vamos analizar o histograma dele :question:

```python
h = QgsRasterHistogramWidget(NDVI)
h.show()
h.computeHistogram(True)
h.refreshHistogram()
h.histoSaveAsImage(path.join(diretorio, "NDVI.png"))
h.destroy()
```  
