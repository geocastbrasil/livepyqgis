# Breve introdução ao python

Á vocês que nunca programaram com python. separamos umas breves linhas, tipo "hands-on" sobre a linguagem. Seria o básico do básico, ou o mínimo necessário para seguir com as lives... Portanto, não substitui um bom curso/livro sobre a linguagem.  

O Qgis possui um terminal integrado onde, através dos comandos, podemos
adicionar camadas e interagir com as camadas existentes.  
Podemos rodar e criar scripts python.  
Para acessá-lo basta irmos em Menu Complementos > Teminal Python  
Atalho: Ctrl + Alt + p  

Digite, na frente do >>>, o seguinte comando  

```python
print( 'Hello World!' )
```

Ou se preferir em Pt_Br:  

```python
print( 'Olá Mundo!' )
```

Parabéns, esse foi o primeiro script rodado no Qgis, simples, não é?!  

> "Mas a vida é uma caixinha de surpresas!". NARRADOR, Joseph Climber.  

Os comandos das lições a seguir serão utilizados no terminal iterativo.
Algumas precauções devem ser tomadas:  

Lembre-se que no python a identação do código conta, então lembre-se de
utilizar sempre que necessário.
Adote um padrão: tabulação, 4 espaços. Utilize-os o tempo todo.

Exemplo:

```python
for i in 'alo':
  print( i ) # antes de digitar a linha, foi pressionado a tecla TAB
```

ou  

```python
for i in algo:
    print( i ) # antes de digitar a linha, foi pressionado a tecla espaço 4x
```

Quando for rodar o script acima a tecla enter deverá ser pressionada 2x para
executar o comando.

Futuramente vamos colocar nossos códigos dentro de um arquivo.py e executar esse script.  
